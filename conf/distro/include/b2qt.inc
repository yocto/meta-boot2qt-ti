############################################################################
##
## Copyright (C) 2022 The Qt Company Ltd.
## Contact: https://www.qt.io/licensing/
##
## This file is part of the Boot to Qt meta layer.
##
## $QT_BEGIN_LICENSE:GPL$
## Commercial License Usage
## Licensees holding valid commercial Qt licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and The Qt Company. For licensing terms
## and conditions see https://www.qt.io/terms-conditions. For further
## information use the contact form at https://www.qt.io/contact-us.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3 or (at your option) any later version
## approved by the KDE Free Qt Foundation. The licenses are as published by
## the Free Software Foundation and appearing in the file LICENSE.GPL3
## included in the packaging of this file. Please review the following
## information to ensure the GNU General Public License requirements will
## be met: https://www.gnu.org/licenses/gpl-3.0.html.
##
## $QT_END_LICENSE$
##
############################################################################

DEPLOY_CONF_NAME:am62xx = "TI SK-AM62"
DEPLOY_CONF_NAME:am62pxx = "TI SK-AM62P"
DEPLOY_CONF_NAME:j784s4-evm = "TI SK-AM69"

IMAGE_FSTYPES += "wic.xz"
DEPLOY_CONF_IMAGE_TYPE = "wic.xz"

QBSP_IMAGE_CONTENT += "\
    ${IMAGE_LINK_NAME}.wic.xz \
    ${IMAGE_LINK_NAME}.conf \
    ${IMAGE_LINK_NAME}.info \
    "

DISTRO_FEATURES:remove = "ld-is-gold"

PREFERRED_PROVIDER_virtual/egl_am62xx = "ti-img-rogue-umlibs"
PREFERRED_PROVIDER_virtual/libgles1_am62xx = "ti-img-rogue-umlibs"
PREFERRED_PROVIDER_virtual/libgles2_am62xx = "ti-img-rogue-umlibs"
PREFERRED_PROVIDER_virtual/libgbm_am62xx = "ti-img-rogue-umlibs"

# Disable EFI boot for now
EFI_PROVIDER:am62xx = ""
MACHINE_FEATURE:remove:am62xx = "efi"
WKS_FILE:am62xx = "sdimage-2part.wks"

# most AM69 boards are hs-fs with ti-img-rogue
SYSFW_SUFFIX:j784s4-evm-k3r5 = "hs-fs"
PREFERRED_PROVIDER_virtual/egl_j784s4-evm-k3r5 = "ti-img-rogue-umlibs"
PREFERRED_PROVIDER_virtual/libgles1_j784s4-evm-k3r5 = "ti-img-rogue-umlibs"
PREFERRED_PROVIDER_virtual/libgles2_j784s4-evm-k3r5 = "ti-img-rogue-umlibs"
PREFERRED_PROVIDER_virtual/libgbm_j784s4-evm-k3r5 = "ti-img-rogue-umlibs"
